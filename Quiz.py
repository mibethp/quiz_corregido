# A resultado de la multiplicación de todos los valores
valores={
    "item1":3,
    "item2":2,
    "item3":8,
    "item4":10,
    "item5":8,
    "item6":7,
    "item7":11,
    "item8":3,
    "item9":2,
    "item10":13}

s= 1
    
for total in valores:
    s= s*valores[total]
    print(s)
   
# B

diccionario={

   "item1":3,
    "item2":2,
    "item3":8,
    "item4":10,
    "item5":8,
    "item6":7,
    "item7":11,
    "item8":3,
    "item9":2,
    "item10":13,

}

t = []
r = []
e= []

for h in diccionario.values():
    r.append(h)

for h in r:
    if h not in t:
        t.append(h)

total = 1

for h in t:
    repeticiones = r.count(h)
    if repeticiones >= 2:
        total += 1
        for a in range(repeticiones):
            e.append(h)

if total >= 2:
    print("duplicados:")

for h in e:
    print(h)

#c
mi_diccionario={

   "item1":3,
    "item2":2,
    "item3":8,
    "item4":10,
    "item5":8,
    "item6":7,
    "item7":11,
    "item8":3,
    "item9":2,
    "item10":13,
}
resultado={}

p=int(input("Ingrese un Número: "))


for z in mi_diccionario:
    if mi_diccionario[z]!=p:
     resultado.setdefault(z, mi_diccionario[z])

print(resultado)

#2.
from collections import Counter

dict_1 = {'w': 100, 'x': 200, 'y': 300}
dict_2 = {'x': 200, 'y': 100, 'z': 100}

resultado = Counter(dict_1) + Counter(dict_2)

print(resultado)



#3
from itertools import product
letras = { "item1": ["a","c"],
          "item2": ["z","h"],
          "item3": ["o","m"]}

for x in product(*[letras[k] for k in sorted(letras.keys())]):
   print("".join(x))

